# Auto-Scalable Web Application Solution
Solution for auto-scalable web application, which scales up and down based on the website traffic. 

## Architecture
![Architecture](/Architecture.png "Architecture")

## Configuring Your Environment's Auto Scaling Group
You can configure how Amazon EC2 Auto Scaling works by editing Capacity on the environment's Configuration page in the environment management console.
### To configure scheduled actions in the Elastic Beanstalk console
* Open the Elastic Beanstalk console.
* Navigate to the management page for your environment.
* Choose Configuration.
* On the Capacity configuration card, choose Modify.  
![config capacity](https://docs.google.com/uc?id=1iW2w8aAUu6AdGy2021RdIBAqHVhplCLq "config capacity")
* In the Auto Scaling Group section, configure the following settings.
    * Environment type – Select Load balanced.
    * Min instances – The minimum number of EC2 instances that the group should contain at any time. The group starts with the minimum count and adds instances when the scale-up trigger condition is met.
    * Max instances – The maximum number of EC2 instances that the group should contain at any time.
        * Note  
        If you use rolling updates, be sure that the maximum instance count is higher than the Minimum instances in service setting for rolling updates.
    * Availability Zones – Choose the number of Availability Zones to spread your environment's instances across. By default, the Auto Scaling group launches instances evenly across all usable zones. To concentrate your instances in fewer zones, choose the number of zones to use. For production environments, use at least two zones to ensure that your application is available in case one Availability Zone goes out.
    * Placement (optional) – Choose the Availability Zones to use. Use this setting if your instances need to connect to resources in specific zones, or if you have purchased reserved instances, which are zone-specific. If you also set the number of zones, you must choose at least that many custom zones.
    * If you launch your environment in a custom VPC, you cannot configure this option. In a custom VPC, you choose Availability Zones for the subnets that you assign to your environment.
    * Scaling cooldown – The amount of time, in seconds, to wait for instances to launch or terminate after scaling, before continuing to evaluate triggers. For more information, see Scaling Cooldowns.  
![config capacity](https://docs.google.com/uc?id=1oxQCAIyMBL3mkiTxlyHhcmOpGiNvq00z "config capacity")
* Choose Apply.

## Configuring Auto Scaling Triggers
You can configure the triggers that adjust the number of instances in your environment's Auto Scaling group in the Elastic Beanstalk console.
### To configure triggers in the Elastic Beanstalk console
* Open the Elastic Beanstalk console.
* Navigate to the management page for your environment.
* Choose Configuration.
* On the Capacity configuration card, choose Modify.
* In the Scaling triggers section, configure the following settings:
    * Metric – Metric used for your Auto Scaling trigger.
    * Statistic – Statistic calculation the trigger should use, such as Average.
    * Unit – Unit for the trigger metric, such as Bytes.
    * Period – Specifies how frequently Amazon CloudWatch measures the metrics for your trigger.
    * Breach duration – Amount of time, in minutes, a metric can be outside of the upper and lower thresholds before triggering a scaling operation.
    * Upper threshold – If the metric exceeds this number for the breach duration, a scaling operation is triggered.
    * Scale up increment – The number of Amazon EC2 instances to add when performing a scaling activity.
    * Lower threshold – If the metric falls below this number for the breach duration, a scaling operation is triggered.
    * Scale down increment – The number of Amazon EC2 instances to remove when performing a scaling activity.
![Configuring Auto Scaling Triggers](https://docs.google.com/uc?id=1Ktoyxq8Kgh8pJw8vgOrdFFzw9w80AFRK "Configuring Auto Scaling Triggers")
    * Choose Apply.

## Components Description
### Elastic Beanstalk
AWS Elastic Beanstalk is an easy-to-use service for deploying and scaling web applications and services developed with Java, .NET, PHP, Node.js, Python, Ruby, Go, and Docker on familiar servers such as Apache, Nginx, Passenger, and IIS.
### Route53
Amazon Route 53 is a highly available and scalable cloud Domain Name System (DNS) web service. It is designed to give developers and businesses an extremely reliable and cost effective way to route end users to Internet applications by translating names like www.example.com into the numeric IP addresses like 192.0.2.1 that computers use to connect to each other. Amazon Route 53 is fully compliant with IPv6 as well.
### Elastic Load Balancer
Elastic Load Balancing automatically distributes incoming application traffic across multiple targets, such as Amazon EC2 instances, containers, and IP addresses. It can handle the varying load of your application traffic in a single Availability Zone or across multiple Availability Zones.
### EC2 Instance
Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides secure, resizable compute capacity in the cloud.
### Availability Zone
Amazon EC2 is hosted in multiple locations world-wide. These locations are composed of regions and Availability Zones. Each region is a separate geographic area. Each region has multiple, isolated locations known as Availability Zones.
### S3 bucket
Object storage built to store and retrieve any amount of data from anywhere.
### DynamoDB
Nonrelational database for applications that need performance at any scale.
### Redis
Fast, open source in-memory data store for use as a database, cache, message broker, and queue.
### CloudWatch
Amazon CloudWatch is a monitoring and management service built for developers, system operators, site reliability engineers (SRE), and IT managers.
### Auto Scaling
Amazon EC2 Auto Scaling helps you maintain application availability and allows you to dynamically scale your Amazon EC2 capacity up or down automatically according to conditions you define.
### Auto Scaling Groups
An Auto Scaling group contains a collection of EC2 instances that share similar characteristics and are treated as a logical grouping for the purposes of instance scaling and management.

## Reference
* [AWS Documentation](https://docs.aws.amazon.com)